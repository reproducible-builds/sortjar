package org.briarproject.sortjar;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

public class SortJar {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		if(args.length < 2 || args.length > 3 || args[0].equals(args[1])) {
			System.err.println("Usage: SortJar <in.jar> <out.jar> [date]");
			System.exit(1);
		}
		long timestamp = 0;
		if(args.length == 3) {
			String date = args[2];
			try {
				timestamp = Date.parse(date);
				System.out.println("Setting timestamp to " + date);
			} catch(IllegalArgumentException e) {
				System.err.println("Could not parse date: " + date);
				System.exit(2);
			}
		}
		String input = args[0], output = args[1];
		try {
			// Read and sort the entries
			Map<String, JarEntry> entries = new TreeMap<String, JarEntry>();
			JarFile in = new JarFile(input);
			for(JarEntry e : Collections.list(in.entries()))
				entries.put(e.getName(), e);
			// Write the sorted entries
			JarOutputStream out = new JarOutputStream(
					new FileOutputStream(output));
			for(JarEntry e : entries.values()) {
				JarEntry e1 = new JarEntry(e.getName());
				e1.setTime(timestamp);
				out.putNextEntry(e1);
				InputStream entryIn = in.getInputStream(e);
				int read;
				byte[] buf = new byte[4096];
				while((read = entryIn.read(buf, 0, buf.length)) != -1)
					out.write(buf, 0, read);
				out.closeEntry();
				entryIn.close();
			}
			out.close();
			in.close();
			System.out.println("Sorted " + entries.size() + " entries");
		} catch(IOException e) {
			e.printStackTrace();
			System.exit(3);
		}
	}
}
